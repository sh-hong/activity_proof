# Activity-Proof

## Introduction

Activity-Proof is a documentary activity proofing system based on Ethereum.

it provides an easier way of proofing their activities, and helps issuers issue credential on their own without any help.

## Feature

- Holder can record their activity and manage issued credentials.

- Holder can prove activities by giving credential-keys with correct issuer address to verifier.

- Issuer can issue credentials without others help.

- Verifier can fetch the credentials from blockchain by keys provided by holder and verify them.

## Related Projects

- working
