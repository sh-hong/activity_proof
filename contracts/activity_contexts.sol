pragma solidity ^0.4.0;

contract ActivityContexts {
    struct Context {
        bytes32 id;
        string meta;
    }

    mapping(bytes32 => Context) contextMap;
    Context[] contextList;
    uint totalContext;

    function addContext(string memory _meta) public {
        bytes32 id = keccak256(abi.encodePacked(_meta));
        ActivityContexts context = ActivityContexts(id, _meta);
        contextMap[id] = context;
        contextList.push(context);
        totalContext += 1;
    }

    function getContext(bytes32 _id) public view returns (string memory) {
        for (uint i = 0; i < totalContext; i++) {
            if (_id == contextList[i].id) return contextList[i].meta;
        }
        return '';
    }

    function getContextByIndex(uint _index) public view returns (string memory) {
        if (totalContext <= _index) revert();
        return contextList[_index].meta;
    }
}
