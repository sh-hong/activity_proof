pragma solidity ^0.4.24;

contract ActivityIssuer {
    struct Issue {
        bytes32 id;
        string context; // "[0x43ba1f...1bb, 0xff12acb3...bcc]"
        string subject;
        string issDate;
        string expDate;
        bytes20 xHolder;
        address issuer;
        string fPath;
    }

    Issue[] issues;
    uint totalIssue;
    address issuer;

    constructor() public {
        issuer = msg.sender;
    }

    modifier isIssuer {
        if (issuer == msg.sender) _;
    }

    function getIssue(bytes32 _id) public view returns (bytes32, string memory, string memory, string memory, string memory, bytes20, address, string memory) {
        for (uint i = 0; i < totalIssue; i++) {
            if (issues[i].id == _id) {
                Issue memory s = issues[i];
                return (s.id, s.context, s.subject, s.issDate, s.expDate, s.xHolder, s.issuer, s.fPath);
            }
        }
        return (0, "", "", "", "", 0, 0, "");
    }

    function addIssue(bytes32 _id, string memory _context, string memory _subject, string memory _issDate, string memory _expDate,
        bytes20 _xHolder, string memory _fPath) public isIssuer {
        Issue memory issue = Issue(_id, _context, _subject, _issDate, _expDate, _xHolder, msg.sender, _fPath);
        issues.push(issue);
        totalIssue += 1;
    }

    function updateIssuer(address _newIssuer) public isIssuer returns (bool) {
        issuer = _newIssuer;
        return true;
    }

    function updateFilePath(bytes32 _id, string memory _newFilePath) public isIssuer returns (bool) {
        for (uint i = 0; i < totalIssue; i++) {
            if (issues[i].id == _id) {
                issues[i].fPath = _newFilePath;
                return true;
            }
        }
        return false;
    }
}
