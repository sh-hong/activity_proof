pragma solidity ^0.4.24;

contract ActivityProof {
    struct Holder {
        address owner;
        bytes mapper;
    }

    struct Record {
        bytes32 key;
        string memo;
    }

    struct RecordSet {
        Record[] records;
        uint size;
    }

    struct IssuerMeta {
        address owner;
        string name;
        address contractAddress;
    }

    mapping(address => Holder) holders;
    uint totalHolder;
    mapping(address => RecordSet) recordSet;
    IssuerMeta[] issuers;
    uint totalIssuer;


    function addHolder(bytes _mapper) public {
        holders[msg.sender] = Holder(msg.sender, _mapper);
    }

    function removeHolder() public {
        delete holders[msg.sender];
        for (uint i = 0; i < recordSet[msg.sender].size; i++) {
            delete recordSet[msg.sender].records[i];
        }
        recordSet[msg.sender].size = 0;
    }

    function addRecord(bytes32 _key, string memory _memo) public {
        Record memory record = Record(_key, _memo);
        _addRecord(record);
    }

    function removeRecord(bytes32 _key) public returns (bool) {
        for (uint i = 0; i < recordSet[msg.sender].size; i++) {
            if (recordSet[msg.sender].records[i].key == _key) {
                _removeRecord(i);
                return true;
            }
        }
        return false;
    }

    function getRecord(address _who, uint _index) public view returns(bytes32, string memory) {
        return (recordSet[_who].records[_index].key, recordSet[_who].records[_index].memo);
    }

    function addIssuerMeta(string memory _name, address _contractAddress) public {
        for (uint i = 0; i < totalIssuer; i++) {
            if (issuers[i].contractAddress == _contractAddress) revert();
        }
        issuers.push(IssuerMeta(msg.sender, _name, _contractAddress));
        totalIssuer += 1;
    }

    function removeIssuerMeta(string memory _contractAddress) public {
        for (uint i = 0; i < totalIssuer; i++) {
            if (issuers[i].contractAddress == _contractAddress) {
                if (issuers[i].owner == msg.sender) {
                    delete issuers[i];
                    return;
                }
                revert();
            }
        }
    }

    function getIssuerMeta(string memory _name) public view returns (address) {
        for (uint i = 0; i < totalIssuer; i++) {
            bytes32 hName = keccak256(abi.encodePacked(issuers[i].name));
            bytes32 hGivenName = keccak256(abi.encodePacked(_name));
            if (hName == hGivenName) return issuers[i].contractAddress;
        }
        return 0;
    }

    function _addRecord(Record memory _record) private {
        recordSet[msg.sender].records.push(_record);
        recordSet[msg.sender].size += 1;
    }

    function _removeRecord(uint _recordIndex) private {
        delete recordSet[msg.sender].records[_recordIndex];
        // recordSet[msg.sender].size -= 1;
    }
}
